---
layout: job_page
title: "Security Engineer"
---

As a member of the security team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams to provide guidance on security best practices.

The [Security Team](/handbook/engineering/security) is responsible for leading and
implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities

- Develop security training and guidance to internal development teams
- Assist with recruiting activities and administrative work
- Communication
  * Handle communications with independent vulnerability researchers and triage reported abuse cases.
  * Educate other developers on anti-abuse cases, workflows and processes.
  * Ability to professionally handle communications with outside researchers, users, and customers.
  * Ability to communicate clearly on anti-abuse issues.
  
## Requirements

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values

### Senior Analyst

* Leverages security expertise in at least one specialty area
* Triages and handles/escalates security issues independently
* Conduct reviews and makes recommendations
* Great written and verbal communication skills
* Screen security candidates during hiring process

### Anti-Abuse Analyst

The Anti-Abuse Analyst is responsible for leading and implementing the various initiatives that relate to improving GitLab's security including:

* Handle tickets/requests escalated to abuse
* Handle DMCA, phishing, malware, botnet, intrusion attempts, DoS, port scanning, spam, spam website, PII and web-crawling abuse reports to point of mitigation of abuse
* Verify proper classification of incoming abuse reports
* Execute messaging to customers on best practices
* Monitoring email, forums, and other communication channels for abuse, and responding accordingly
* Assist with recruiting activities and administrative work
* Making sure internal knowledge reference pages are updated

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with Security Engineer
- Candidates will then be invited to schedule an interview with Director of Security
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
