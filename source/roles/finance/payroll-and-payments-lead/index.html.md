---
layout: job_page
title: "Payroll and Payments Lead"
---

GitLab is adding the next essential member who can help drive operational improvements within our finance department. You will join our team in its early stages responsible for developing a highly efficient, world class accounting process. This person will administer and improve our global payroll and payments process. You will be responsible for 100% of payments to GitLab contributors globally.

## Responsibilities

- Coordinate all global payroll processing operations. Currently the company runs payroll in five countries and has partners in another three countries that provide payroll solutions to contributors.
- Ensure that all relevant data is processed and track through the relevant payroll systems.
- Responsible for working with local tax and legal advisors to ensure the company follows local regulations with respect to payroll.
- Work closely with GitLab contributors and the People Ops team to ensure timely response to questions and requests that have been processed.
- Complete filings for payroll subsidies, where applicable.
- Accounting lead for implementation of new payroll system in the US.
- Accounting lead for other global payroll systems.
- Establish and enforce proper accounting policies and principles.
- Primary ownership for management and external reporting.
- Development and implementation of new procedures to enhance the workflow of payments to contributors.
- Primary contact point for external audit with respect to payroll matters.
- Coordinate with other auditors as needed.
- Support overall department goals and objectives.

## Requirements

- Proven work experience as a payroll administrator.
- Experience with large payroll platforms such as ADP or Paychex.
- Been responsible for at least one international payroll function.
- Public company experience with Sarbanes Oxley a plus.
- Experience with Netsuite a plus.
- Proficient with google sheets.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
* Next, candidates will be invited to schedule a first interview with our Controller.
* Next, candidates will be invited to schedule an interview with a People Business Partner.
* Candidates will then be invited to schedule an interview with our CFO.
* Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](handbook/hiring).
