---
layout: markdown_page
title: Prioritizing Tickets as a Support Engineer
---

### On this page
{:.no_toc}

- TOC
{:toc}

## Service and Support we Provide

For an overview of the support we provide to customers and GitLab.com users, please see the general [support page](/support/). What follows is a more detailed description of the level of service.

## Service Level Agreements
{: #sla}

The channels are sorted in order of priority, there are 4 SLA categories:

| SLA | Channel                | Response Time                          |
|-----|----------------------------|----------------------------------------|
| 1   | [Emergencies (Premium Customers only)](/handbook/support/channels/#emergency-tickets)                | 30 minutes                             |
| 2   | [Premium Customer - Regular Tickets](/handbook/support/channels/#regular-zendesk-tickets)                | 4 hrs (business)                             |
| 3   | [Regular Tickets](/handbook/support/channels/#regular-zendesk-tickets) and [Security](/handbook/support/channels/#security-disclosures) | 1 business day                         |

**Response time indicates the first reply time.**

Preferably we like to answer tickets sooner than the SLA requires. The higher a channel is in the list the sooner it should be answered.

The above SLAs are based on ticket priority which can be set manually by support agents. See [setting ticket priority](/handbook/support/workflows/zendesk/setting_ticket_priority.html)

### How We prioritize tickets

Internally to support, we use Zendesk as our ticket management tool. We've defined our queing priority as such:

+ High:
  - 5xx errors in a production environment
  - GitLab Components (Unicorn/Gitaly/Postgres/Redis) Down
  - Slow performance affecting a majority of users for that instance

+ Normal:

+ Low:

## SLA Workflow

Support Engineers should work on tickets within their assigned support speciality as a first priority. Tickets should be picked up in the following order to make sure that SLAs are not breached, and customers receive the proper level of service:

1. Important Tickets
1. Premium Tickets Near Breaching
1. Premium Tickets Breached
1. Enterprise Edition Near Breaching
1. Enterprise Edition Breached

After these are addressed tickets should be worked on in Priority Order. When a ticket is breaching or has breached its first reply (or next reply) SLA this ticket must be picked up by any Support Engineer independently of who is assigned to it. This also applies to tickets for Resellers (i.e. anyone picks up if close to breaching, regardless of who the Dedicated Support Engineer is).

### Important Tickets

This is a special view that is used for two purposes:

1. Triggered Emergencies by customers. These will page the on call, show up in slack, and always be at the top of the queue.
1. Strategic Issues as deemed by the support lead. During large customer onboardings or other times where we may need to provide heightened response times the Support Lead may escalate tickets to important status. If so, this will be communicated on a team call. This is an extraordinary measure, and there should be no more than 3-4 organizations this applies to at any given time.


### Hot Queue: How GitLab Manages Ticket Assignments

Support teams often use an "assignment" model, where an agent is assigned a ticket and sees the ticket through to completion. If they are stuck, they can re-assign this ticket to another agent to pick up and try and complete. At GitLab however, we're taking advantage of our global support team and using a system dubbed "Hot Queuing". This system means that we all work from one global queue and it's expected that multiple agents will be working together on most issues. This allows us to do the following:

+ Keep tickets moving forward 24/7.
+ Exposes the team to more issues so more learning is happening.
+ Provide faster response to customers since tickets are not hidden away in an agent's personal queue and "bound" to an agent's workday.
+ Everyone has an accurate global representation of the support load.

#### When Should Tickets be Assigned?

There are times in the support process that an agent will want to assign the ticket to themselves:

+ If you are going to take the ticket to a call/screenshare, take it out of the queue by assigning the ticket to yourself. After the call, if the ticket is still on-going, feel free to unassign yourself so that everyone can continue to move it forward.
+ If you are committed to solving the problem strategically. There are times where we want this, and agents have full discretion to choose that, but it should be few and far between.

#### What if I Can't Answer a Ticket?

If you see a ticket in the queue that you are not able to answer, you should:

+ Make an internal note with your gut feelings as to where the issue might be.
+ CC yourself so you can follow along as the ticket progresses.
+ If you know that agents will need logs or version information, feel free to start with a simple response asking for such.
+ Consider asking about the ticket in slack to see if others can help.
+ Consider asking a more senior colleague, or take a look at the [Team page](/team/) to see who can help you.

With the hot queue, we all work together and no one should be scared to start a ticket.

### Zendesk SLA settings and Breach Alerts

SLAs are set as Business Rules within Zendesk. For more information, please refer to the specific [Zendesk](/handbook/support/workflows/zendesk/zendesk_admin.html) page.

We have a slack integration that will notify us of a Premium ticket that will breach within an hour. If you see one of these, start a thread and consider this a _small emergency_. If you need help, draw attention of other support engineers by tagging them, and work to move the ticket forward.

>If customer's reply is the last one in the ticket, do not set it to the Pending status silently. Because of that, breach clock will be lost if customer replies to the ticket again later.
Instead, send some confirmation, greetings, etc together with changing the status.