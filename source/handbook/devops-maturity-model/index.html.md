---
layout: markdown_page
title: "Simplified DevOps Maturity Model"
---

## On this page
{:.no_toc}

- TOC
{:toc}

As organizations adopt DevOps practices, they typically select a series of DevOps practices and improve along a maturity curve, where specific practices and processes are improved first, followed by others. No organization goes from zero to fully mature DevOps in one step, it's a journey of continuous improvement. 

Typical maturity models are complex, multidimensional constructs which are used to measure maturity, plan improvements and in some cases document the organizational effectiveness. The traditional 5 level model (Initial, Managed, Defined, Quantitatively Managed, Optimizing) is perhaps overly complex for describing the maturity of an organization on their DevOps Adoption.

We can define a simpler model, something similar to this three-level model for Continuous Delivery Maturity: 
[http://bekkopen.github.io/maturity-model/](http://bekkopen.github.io/maturity-model/).

This simplified DevOps Maturity Model descibes a typical sequence of evolving DevOps maturity where the focus started with improving development practices, then focuses on how they deploy and release consistently, finally focusing on how the organization optimizes the end-to-end lifecycle.

## Simplified DevOps Maturity Model 

| **Adopting** | **Scaling** | **Optimizing** |
|---|---|---|
| Starting to adopt DevOps, development teams focus on their core software development practices to streamline and accelerate their ability to organize and create software. Specifically, they focus on adopting modern development practices, source code management, automating build and testing to make their work more consistent and responsive to change. | Building on stable, iterative, and repeatable development practices, teams scale and extend these practices to address the challenges of delivering code to production, bridging the gap with automated configuration, deployment, and release of applications to end users. | With a maturing DevOps lifecycle, teams focus on optimizing their business value, leveraging end-to-end feedback, monitoring, and insight to streamline and improve DevOps practices. They identify and address constraints, such as security testing, where they tune and improve their processes; increasing velocity, quality, and security. |
|                        |                       |                  |
| **Primary emphasis** for teams adopting DevOps.    | **Primary emphasis** for teams scaling their DevOps transformation.      | **Primary emphasis** for organizations optimizing the value from their DevOps lifecycle.     |
| Consistent **modern development** processes (typically agile) and collaboration to keep the team in sync with the project goals and changing business priorities.   | Consistently packaging and managing their binary assets makes it easy to deploy the right version to the right environment.  | Understanding cross team and cross organizational issues to optimize and manage their **portfolio of projects**         |
| Sharing common code repository for **distributed version control** and branches so that team members have access to the most recent version of their code.    | Automating the deployment and configuration of the application to ensure consistent, repeatable, and fast deploys.   | Organizations develop and deploy dashboards to monitor and track key business metrics and KPIs so they can remove constraints that slow down value delivery.     |
| **Automating build and testing** to ensure their code is consistently built and tested.   | Extend automated testing to incorporate **extensive functional, performance, and acceptance** testing.    | Many organizations realize that a common constraint is Security Testing, which is often deferred to late phases in the development project.  This often results in re-work and delays.   A strategy to address the constraint is to **shift security testing left** and make it part of every build.  |
| Adopting **continuous integration to ensure each commit is built, tested, and validated.**   |             |             |
| Collecting data and insight about the effectiveness of their development processes.   | Gathering data and insight about the effectiveness of the delivery processes.  | Continuous improvement practices to analyze the **value stream** and identify opportunities to improve and optimize the flow from Idea to Production.    |