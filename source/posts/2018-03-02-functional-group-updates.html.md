---
title: "GitLab's Functional Group Updates: February 19th - March 2nd" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: GitLab news
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1LXszeSea9EE5Pq1i-NuFevb234k5tA4Hd3Sl2oOKAkA/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/n_sPmmdTu20" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/product-update-feb-20)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EFrQBFgBJFc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Security Products Team

[Presentation slides](https://docs.google.com/presentation/d/1zLomMVyWTXzZ4M7d6hxg3n2N3JLMTqXEmGYeS9uoyvU/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Q4qmCGIKgjM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Legal Team

[Presentation slides](https://docs.google.com/presentation/d/1jNXLsjMYNefLrJP4qa33_ORLl7URHSrl5ZV3tQ_ouG0/edit?ts=5a8da63e)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qJxOhOYbga4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Platform Team

[Presentation slides](https://docs.google.com/presentation/d/1wibaqQN-yR85SnWN4FwjkAj6eZe0MvlaX9dbMBf3Nmw/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/iofAg6Xs7_Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/16HADBsXZc7O2GfCq7pwaEDjiicO-wkN7SrwBooC_VAw/edit#slide=id.g156008d958_0_18)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/m0F7BhKy3zk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### General Team

[Presentation slides](https://docs.google.com/presentation/d/1Gbs707Knn5euOZgpIz79SmdWjVsOLIKiv0eDz4lkrJ0/edit#slide=id.g3443f09fb2_0_17)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/E6xvE33Xl80" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
